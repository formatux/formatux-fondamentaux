* xref:index.adoc[Éditeur de texte VI]
** xref:index.adoc#introduction[Introduction]
** xref:index.adoc#la-commande-vi[La commande vi]
** xref:index.adoc#mode-opératoires[Modes opératoires]
*** xref:index.adoc#le-mode-commandes[Le mode Commandes]
*** xref:index.adoc#le-mode-insertion[Le mode Insertion]
*** xref:index.adoc#le-mode-ex[Le mode Ex]
** xref:index.adoc#déplacer-le-curseur[Déplacer le curseur]
** xref:index.adoc#insérer-du-texte[Insérer du texte]
** xref:index.adoc#caractères-mots-et-lignes[Caractères, mots et lignes]
** xref:index.adoc#commandes-ex[Commandes EX]
*** xref:index.adoc#numéroter-les-lignes[Numéroter les lignes]
*** xref:index.adoc#rechercher-une-chaîne-de-caractères[Rechercher une chaîne de caractères]
*** xref:index.adoc#remplacer-une-chaîne-de-caractères[Remplacer une chaîne de caractères]
*** xref:index.adoc#opérations-sur-les-fichiers[Opérations sur les fichiers]
** xref:index.adoc#autres-fonctions[Autres fonctions]
** xref:index.adoc#la-commande-vimtutor[La commande vimtutor]
